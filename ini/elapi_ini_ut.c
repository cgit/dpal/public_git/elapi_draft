/*
    ELAPI

    Unit test for configuration reading interface.

    Copyright (C) Dmitri Pal <dpal@redhat.com> 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include "elapi_ini.h"
#include "elapi_tools.h"

int main()
{
    int error;
    struct collection_item *ini_config;

    error = config_to_collection("test", "elapi.conf", "./elapi.d", &ini_config); 
    if(error) {
        printf("Attempt to read configuration returned error: %d\n",error);
        return error;
    }

    debug_collection(ini_config,ELAPI_TRAVERSE_DEFAULT);
    print_collection(ini_config);
    destroy_collection(ini_config);
    return 0;

} 
