/*
    ELAPI

    Header file for reading configuration from INI file 
    and storing as collection. 

    Copyright (C) Dmitri Pal <dpal@redhat.com> 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ELAPI_INI_H
#define ELAPI_INI_H

#include "elapi_collection.h"

#define INI_DEFAULT_SECTION "default"

/* Read configuration ini file and create collection out of it */
int config_to_collection(char *application,                     /* Name of the application that will be used to get config for */
                         char *config_file,                     /* Name of the configuration file with default sessings for all apps */
                         char *config_dir,                      /* Name of the directory where the configuration files for different apps will be dropped */
                         struct collection_item **ini_config);  /* New config object */ 

/* Function to get value from the config */
int get_value_from_config(void *value,                          /* Pointer to the memory that will point to the retrieved result.
                                                                 * The memory will always be preallocated. */ 
                          int type,                             /* Expected type of the result */
                          char *section, 						/* Section to search in */
                          char *name,                           /* Value to look for */
                          struct collection_item *ini_config);  /* Config handle */

/* read ini file and create collection out of it factoring in the env variables */
/* int ini_to_collection_env(char *filename, struct collection_item **ini_config, char *ENV prefix); */



#endif
