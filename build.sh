#!/bin/sh -x

cd ~/IPA/Code/elapi

cd tools
make -f elapi_tools.mak clean
make -f elapi_tools.mak
ret="$?"
echo $ret
if [ "$ret" != "0" ]; then echo "Exiting"; exit 1; fi
cd ..


cd collection
make -f elapi_collection.mak clean
make -f elapi_collection.mak
ret="$?"
echo $ret
if [ "$ret" != "0" ]; then echo "Exiting"; exit 1; fi
cd ..


cd sinks
cd stderr
make -f elapi_sink_stderr.mak clean
make -f elapi_sink_stderr.mak
ret="$?"
echo $ret
if [ "$ret" != "0" ]; then echo "Exiting"; exit 1; fi
cd ..
cd syslog
make -f elapi_sink_syslog.mak clean
make -f elapi_sink_syslog.mak
ret="$?"
echo $ret
if [ "$ret" != "0" ]; then echo "Exiting"; exit 1; fi
cd ..
cd ..

cd dispatcher
make -f elapi_dispatcher.mak clean
make -f elapi_dispatcher.mak
ret="$?"
echo $ret
if [ "$ret" != "0" ]; then echo "Exiting"; exit 1; fi
cd ..


cd ini
make -f elapi_ini.mak clean
make -f elapi_ini.mak
ret="$?"
echo $ret
if [ "$ret" != "0" ]; then echo "Exiting"; exit 1; fi
cd ..
