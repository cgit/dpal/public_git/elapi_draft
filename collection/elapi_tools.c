/*
    ELAPI

    Additional functions for printing and debugging collections.

    Copyright (C) Dmitri Pal <dpal@redhat.com> 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <malloc.h>
#include "elapi_collection.h"
#include "elapi_debug.h"
#include "elapi_util.h"
#include "elapi_tools.h"

/* Debug handle */
int debug_handle(char *property,
                 int property_len,
                 int type,
                 void *data,
                 int length,
                 void *custom_data,
                 int *dummy)
{
    int i;
    int nest_level;

    DEBUG_STRING("debug_handle","Entry.");


    nest_level = *(int *)(custom_data);
    DEBUG_NUMBER("We are getting this pointer:",custom_data);
    DEBUG_NUMBER("Nest level:",nest_level);

    switch(type) {
        case ELAPI_TYPE_STRING:
                                printf("%*s %s[%d] str: %s (%d)\n",
                                       (nest_level-1)*4,"",
                                       property,
                                       length, 
                                       (char *)(data),
                                       (int)(nest_level));
                                break;
        case ELAPI_TYPE_BINARY:
                                printf("%*s %s[%d] bin: ",
                                       (nest_level-1)*4,"",
                                       property,
                                       length);
                                for(i=0;i<length;i++) printf("%02X",*((unsigned char *)(data+i)));
                                printf(" (%d)\n",(int)(nest_level));
                                break;
        case ELAPI_TYPE_INTEGER:
                                printf("%*s %s[%d] int: %d (%d)\n",
                                       (nest_level-1)*4,"",
                                       property,
                                       length, 
                                       *((int *)(data)),
                                       nest_level);
                                break;
        case ELAPI_TYPE_UNSIGNED:
                                printf("%*s %s[%d] uint: %u (%d)\n",
                                       (nest_level-1)*4,"",
                                       property,
                                       length, 
                                       *((unsigned int *)(data)),
                                       nest_level);
                                break;
        case ELAPI_TYPE_LONG:
                                printf("%*s %s[%d] long: %ld (%d)\n",
                                       (nest_level-1)*4,"",
                                       property,
                                       length, 
                                       *((long *)(data)),
                                       nest_level);
                                break;
        case ELAPI_TYPE_ULONG:
                                printf("%*s %s[%d] ulong: %lu (%d)\n",
                                       (nest_level-1)*4,"",
                                       property,
                                       length, 
                                       *((unsigned long *)(data)),
                                       nest_level);
                                break;
        case ELAPI_TYPE_DOUBLE:
                                printf("%*s %s[%d] double: %.4f (%d)\n",
                                       (nest_level-1)*4,"",
                                       property,
                                       length, 
                                       *((double *)(data)),
                                       nest_level);
                                break;
        case ELAPI_TYPE_COLLECTION:
                                nest_level++;                                 
                                printf("%*s %s[%d] header: count %d, ref_count %d data: ",
                                       (nest_level-1)*4,"",
                                       property,
                                       length,
                                       ((struct collection_header *)(data))->count,
                                       ((struct collection_header *)(data))->reference_count);
                                for(i=0;i<length;i++) printf("%02X",*((unsigned char *)(data+i)));
                                printf(" (%d)\n",(int)(nest_level));
                                break;
        case ELAPI_TYPE_COLLECTIONREF:
                                printf("%*s %s[%d] external link: ",
                                       (nest_level-1)*4,"",
                                       property,
                                       length);
                                for(i=0;i<length;i++) printf("%02X",*((unsigned char *)(data+i)));
                                printf(" (%d)\n",(int)(nest_level));
                                break;
        case ELAPI_TYPE_END:
                                nest_level--;
                                /* printf("Reduced nest level\n");*/
                                break;
        default:
                                printf("Not implemented yet.\n");
                                break;
    }
    *(int *)(custom_data) = nest_level;
    DEBUG_NUMBER("Nest level at the end:",nest_level);
    DEBUG_STRING("debug_handle","Success exit.");
    return EOK;
}

/* Print collection for debugging purposes */
int debug_collection(struct collection_item *handle,int flag)
{
    int error = EOK;
    int nest_level=0;

    DEBUG_STRING("debug_collection","Entry.");

    printf("DEBUG COLLECTION %s\n",handle->property);

    flag |= ELAPI_TRAVERSE_END;

    printf("Traverse flags %d\n",flag);

    /* Traverse collection */
    error = traverse_collection(handle,flag,debug_handle,(void *)(&nest_level));
    if(error) printf("Error debuging collection %d\n",error);

    DEBUG_STRING("debug_collection","Exit.");
    return error;
}

/* Print the collection using default serialization */
int print_collection(struct collection_item *handle)
{
    struct serial_data buf_data;
    int error = EOK;

    printf("COLLECTION:\n");

    buf_data.buffer=NULL;
    buf_data.length=0;
    buf_data.size=0;
    buf_data.nest_level=0;

    /* Traverse collection */
    error = traverse_collection(handle,ELAPI_TRAVERSE_DEFAULT | ELAPI_TRAVERSE_END ,serialize,(void *)(&buf_data));
    if(error) printf("Error traversing collection %d\n",error);
    else printf("%s\n",buf_data.buffer);

    free(buf_data.buffer);

    return error;
}


/* Find and print one item using default serialization */
int print_item(struct collection_item *handle, char *name)
{
    struct serial_data buf_data;
    int error = EOK;

    printf("FIND ITEM:\n");

    buf_data.buffer=NULL;
    buf_data.length=0;
    buf_data.size=0;
    buf_data.nest_level=0;

    error =  get_item_and_do(handle, name, ELAPI_TYPE_ANY,ELAPI_TRAVERSE_DEFAULT, serialize,&buf_data);
    if(error) printf("Error searching collection %d\n",error);
    else {
        if(buf_data.buffer != NULL) {
            if(buf_data.length> 0) buf_data.length--;
            *(buf_data.buffer+buf_data.length)= '\0',
            printf("%s\n",buf_data.buffer);
	        free(buf_data.buffer);
        }
        else printf("Name %s is not found in the collection %s.\n",name,handle->property);
    }
    return error;
}


