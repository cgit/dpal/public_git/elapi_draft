/*
    ELAPI

    Header files with serialization methods of the collection.

    Copyright (C) Dmitri Pal <dpal@redhat.com> 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ELAPI_UTIL_H
#define ELAPI_UTIL_H

#include <libxml/xmlwriter.h>
#include "elapi_collection.h"

#define DEFAULT_ENCODING "ISO-8859-1"

#define TEXT_COLLECTION "EVENT"
#define TEXT_COLLEN 5

#define ELEMENT_COLLECTION "MAP"
#define ELEMENT_MEMBER "MEMBER"
#define ATTRIBUTE_NAME "name"
#define ATTRIBUTE_TYPE "type"
#define COLLEN  3 
#define BLOCK_SIZE 1024

#define ELAPI_TYPE_NAME_STRING     "string"
#define ELAPI_TYPE_NAME_BINARY     "bin"
#define ELAPI_TYPE_NAME_INTEGER    "int"
#define ELAPI_TYPE_NAME_UNSIGNED   "uint"
#define ELAPI_TYPE_NAME_LONG       "long"
#define ELAPI_TYPE_NAME_ULONG      "ulong"
#define ELAPI_TYPE_NAME_DOUBLE     "double"
#define ELAPI_TYPE_NAME_UNKNOWN    "unknown"

#define BAD_DATA "NOT UTF-8"


struct serial_data {
    char *buffer;
    int size;
    int length;
    int nest_level;
};


struct xml_data {
    xmlTextWriterPtr writer;
    xmlBufferPtr buf;
    char *given_name;
    int level; 
};


/* Calculate the potential size of the item */ 
int get_data_len(int type, int length);

/* Grow buffer to accomodate more space */
int grow_buffer(struct serial_data *buf_data, int len);

/* Specail function to add different formatting symbols to the output */
int put_marker(struct serial_data *buf_data, void *data, int len);

/* Serialization of data user handler */
int serialize(char *property_in,
              int property_len_in,
              int type,
              void *data_in,
              int length_in,
              void *custom_data,
              int *dummy);

/* Add item's data */
int xml_add(char *property,
            int property_len,
            int type,
            void *data,
            int length,
            void *custom_data,
            int *dummy);

/* Serialize using format */
int serialize_with_format(struct collection_item *event,
                          char *format_str,
                          struct serial_data *data);

#endif
