/*
    ELAPI

    Internal debugging header file.

    Copyright (C) Dmitri Pal <dpal@redhat.com> 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ELAPI_DEBUG_H
#define ELAPI_DEBUG_H

#ifdef ELAPI_LOG_DEBUG
#include <stdio.h>
#endif

#ifdef ELAPI_LOG_DEBUG
#define DEBUG_STRING(message,str) printf("[DEBUG] %23s (%4d) %s %s\n",__FILE__,__LINE__,message,str);
#else
#define DEBUG_STRING(message,str) ;
#endif

#ifdef ELAPI_LOG_DEBUG
#define DEBUG_NUMBER(message,number) printf("[DEBUG] %23s (%4d) %s %lu\n",__FILE__,__LINE__,message,(unsigned long int)(number));
#else
#define DEBUG_NUMBER(message,number) ;
#endif

#ifdef ELAPI_LOG_DEBUG
#define DEBUG_DOUBLE(message,number) printf("[DEBUG] %23s (%4d) %s %e\n",__FILE__,__LINE__,message,(double)(number));
#else
#define DEBUG_DOUBLE(message,number) ;
#endif

#ifdef ELAPI_LOG_DEBUG
#define DEBUG_ASSERT(expression) expression ? ; : printf("ASSERTION FAILED\n");
#else
#define DEBUG_ASSERT(expression) ;
#endif

#ifdef  ELAPI_LOG_DEBUG 
#ifdef      ELAPI_TOOLS_H
#define         DEBUG_COLLECTION(collection) debug_collection(collection,ELAPI_TRAVERSE_DEFAULT);
#else
#define         DEBUG_COLLECTION(collection) ;
#endif
#else
#define     DEBUG_COLLECTION(collection) ;
#endif

#endif
