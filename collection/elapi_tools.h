/*
    ELAPI

    Header file for supplementary functions that provide 
    printing and debugging collections.

    Copyright (C) Dmitri Pal <dpal@redhat.com> 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ELAPI_TOOLS_H
#define ELAPI_TOOLS_H

#include "elapi_collection.h"

/* Debug handle */
int debug_handle(char *property,
                 int property_len,
                 int type,
                 void *data,
                 int length,
                 void *custom_data,
                 int *dummy);

/* Print collection for debugging purposes */
int debug_collection(struct collection_item *handle,int flag);

/* Print the collection using default serialization */
int print_collection(struct collection_item *handle);


/* Find and print one item using default serialization */
int print_item(struct collection_item *handle, char *name);

#endif
