/*
    ELAPI

    Unit test for dispatcher.

    Copyright (C) Dmitri Pal <dpal@redhat.com> 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include "elapi_dispatcher.h"
#include "elapi_collection.h"
#include "elapi_debug.h"
#include "elapi_util.h"
#include "elapi_tools.h"

struct collection_item *event;
struct collection_item *peer;
struct collection_item *socket;
struct collection_item *host;


int build_event()
{
    char binary_dump[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
    int found = 0;

    int error = EOK;

    DEBUG_STRING("build_event","Entry.");

    if((error=create_collection(&peer,"peer")) ||
       (error=add_str_property(peer,NULL,"hostname","peerhost.mytest.com",0)) ||
       (error=add_str_property(peer,NULL,"IPv4","10.10.10.10",12)) || /* Expect trailing zero to be truncated */
       (error=add_str_property(peer,NULL,"IPv6","bla:bla:bla:bla:bla:bla",0))) {
        printf("Failed to add property. Error %d",error);
        destroy_collection(peer);
        return error;
    }

    if((error=create_collection(&host,"host")) ||
       (error=add_str_property(host,NULL,"hostname","myhost.mytest.com",0)) ||
       (error=add_str_property(host,NULL,"IPv4","20.20.20.20",13)) ||
       (error=add_str_property(host,NULL,"IPv6","bla:bla:bla:bla:bla:bla",0))) {
        printf("Failed to add property. Error %d",error);
        destroy_collection(peer);
        destroy_collection(host);
        return error;
    }


    if((error=create_collection(&socket,"socket")) ||
       (error=add_int_property(socket,NULL,"id",1)) ||
       (error=add_long_property(socket,NULL,"packets",100000000L)) ||
       (error=add_binary_property(socket,NULL,"stack",binary_dump,sizeof(binary_dump)))) {
        destroy_collection(peer);
        destroy_collection(host);
        destroy_collection(socket);
        printf("Failed to add property. Error %d\n",error);
        return error;
    }

    /* Embed peer host into the socket2 as reference */    
    error = add_collection_to_collection(socket,NULL,"peer",peer,ELAPI_ADD_MODE_REFERENCE);
    if(error) {
        destroy_collection(peer);
        destroy_collection(host);
        destroy_collection(socket);
        printf("Failed to create collection. Error %d\n",error);
        return error;
    }

    /* build event */
    if((error=create_collection(&event,"event")) ||
       (error=add_str_property(event,NULL,"escape1","ds\\sd=ewrw===sada",0)) ||
       (error=add_str_property(event,NULL,"escape2","dss,d,=,ewrw===sada",0)) ||
       (error=add_str_property(event,NULL,"escape3","ds\\sd=ewrw,()===sada",0)) ||
       (error=add_str_property(event,NULL,"escape4","dssd=ewrw===))))sada",0)) ||
       (error=add_str_property(event,NULL,"escape5","d\\ss(((((d=ew(()()()(),(,,\\\\\\\\rw===sada",0)) ||
       (error=add_int_property(event,NULL,"something",100))) {
        destroy_collection(peer);
        destroy_collection(host);
        destroy_collection(socket);
        destroy_collection(event);
        printf("Failed to create collection. Error %d\n",error);
        return error;
    }

    /* Add host to event */
    error = add_collection_to_collection(event,NULL,NULL,host,ELAPI_ADD_MODE_REFERENCE);
    if(error) {
        destroy_collection(peer);
        destroy_collection(host);
        destroy_collection(socket);
        destroy_collection(event);
        printf("Failed to add collections. Error %d\n",error);
        return error;
    }

    error = add_collection_to_collection(event,NULL,NULL,socket,ELAPI_ADD_MODE_REFERENCE);
    if(error) {
        destroy_collection(peer);
        destroy_collection(host);
        destroy_collection(socket);
        destroy_collection(event);
        printf("Failed to add collections. Error %d\n",error);
        return error;
    }

    debug_collection(event,ELAPI_TRAVERSE_DEFAULT);

    return EOK;
} 

int other_high_level_test()
{
    int error = EOK;
    char *sinks[]= { "syslog", "stderr", "file", NULL };
    char bin[] = { 1, 2, 3, 4, 5, 6, 7, 8 };

    printf("Second high level test start\n");

    printf("%s","=================\nOpening audit\n");
    error = open_audit("my_app",sinks);
    if(error) {
        printf("open_audit returned %d\n", error);
        return error;
    }
    else printf("Success : %d\n",error);

    printf("%s","=================\nCreating collection\n");
    if((error=create_event(&event,"test_event")) ||
       (error=add_str_property(event,NULL,"name","some name",0)) ||
       (error=add_int_property(event,NULL,"number",-100)) ||
       (error=add_binary_property(event,NULL,"bin",bin,8)) ||
       (error=add_double_property(event,NULL,"double",3.141592))) {
        printf("Failed to create collection. Error %d\n",error);
        return error;
    }
    else printf("Success : %d\n",error);

    error = construct_event(&event,"test_event"," %b(bin),"
                                                " %d(int_number),"
                                                " %u(unsigned_number),"
                                                " %ld(long_number),"
                                                " %lu(long_unsigned_number),"
                                                " %s(just_string),"
                                                " %*s(sub_string),"
                                                " %e(double_number)",
                                                bin,8,
                                                -200,
                                                300,
                                                -1234567,
                                                1234567879,
                                                "string",
                                                "truncated string", 10, /* Expect word truncated */
                                                3.141592 * 3);
    if(error) {
        printf("Failed to construct event %d\n", error);
        return error;
    }
    else printf("Success : %d\n",error);


    printf("%s","=================\nLog event using NULL format\n");
    log_event(NULL, event);

    printf("%s","=================\nLog event using format\n");
    log_event("%(stamp), %s(sub_string), %(int_number), %(unsigned_number), %(long_unsigned_number), %(bin), %e(double_number)", event);

    error = modify_event(event,1," %d(int_number),"
                                 " %u(unsigned_number),",
                                 -220,
                                 330);

    if(error) {
        printf("Failed to modify event %d\n", error);
        return error;
    }
    else printf("Success : %d\n",error);

    printf("%s","=================\nLog modified event using NULL format\n");
    log_event(NULL, event);

    printf("%s","=================\nLog modified event using format\n");
    log_event("%(stamp), %s(sub_string), %(int_number), %(unsigned_number), %(long_unsigned_number), %(bin), %e(double_number)", event);

    error = modify_event(event,0," %d(int_number),"
                                 " %u(unsigned_number),",
                                 -250,
                                 350);
    if(error) {
        printf("Failed to modify event %d\n", error);
        return error;
    }
    else printf("Success : %d\n",error);

    printf("%s","=================\nLog modified event using NULL format\n");
    log_event(NULL, event);

    printf("%s","=================\nLog modified event using format\n");
    log_event("%(stamp), %s(sub_string), %(int_number), %(unsigned_number), %(long_unsigned_number), %(bin), %e(double_number)", event);

    destroy_collection(event);
    close_audit();

    printf("Low level test end\n");
    return error;
}


int high_level_test(char *app)
{
    int error = EOK;
    char *sinks[]= { "foo", "stderr", "file", NULL };
    char bin[] = { 1, 2, 3, 4, 5, 6, 7, 8 };

    printf("High level test start\n");

    printf("%s","=================\nOpening audit\n");
    error = open_audit(app,sinks);
    if(error) {
        printf("open_audit returned %d\n", error);
        return error;
    }
    else printf("Success : %d\n",error);

    printf("%s","=================\nAdding syslog - first time - expect success\n");
    error = alter_audit_dispatcher(get_dispatcher(),"syslog",ELAPI_SINK_ACTION_ADD);
    if(error) {
        printf("Expected success got failure %d\n",error);
        return error;
    }
    else printf("Success : %d\n",error);

    printf("%s","=================\nAdding syslog again\n");
    error = alter_audit_dispatcher(get_dispatcher(),"syslog",ELAPI_SINK_ACTION_ADD);
    if(error == 0) {
        printf("Expected failure got success %d\n",error);
        return EINVAL;
    }
    else printf("Expected error : %d\n",error);

    printf("%s","=================\nDeleting stderr\n");
    error = alter_audit_dispatcher(get_dispatcher(),"stderr",ELAPI_SINK_ACTION_DELETE);
    if(error) {
        printf("Expected success got failure %d\n",error);
        return error;
    }
    else printf("Success : %d\n",error);

    printf("%s","=================\nAdding stderr back\n");
    error = alter_audit_dispatcher(get_dispatcher(),"stderr",ELAPI_SINK_ACTION_ADD);
    if(error) {
        printf("Expected success got failure %d\n",error);
        return error;
    }
    else printf("Success : %d\n",error);

    printf("%s","=================\nCreating collection\n");
    if((error=create_event(&event,"test_event")) ||
       (error=add_str_property(event,NULL,"name","some name",0)) ||
       (error=add_int_property(event,NULL,"number",-100)) ||
       (error=add_binary_property(event,NULL,"bin",bin,8)) ||
       (error=add_double_property(event,NULL,"double",3.141592))) {
        printf("Failed to create collection. Error %d\n",error);
        return error;
    }
    else printf("Success : %d\n",error);

    printf("%s","=================\nLog event using NULL format\n");
    log_event(NULL, event);

    printf("%s","=================\nLog event using First format\n");
    log_event("%(stamp), %s(name), %(number), %(bin), %%, %e(double)", event);

    printf("%s","=================\nLog event using Second format\n");
    log_event("%08X(time), %50s(name), %u(number), %s(bin), %%, %A(double)", event);

    printf("%s","=================\nLog event using Third format\n");
    log_event("%-50s(stamp), %50s(name), %lo(number), %a(bin), %%, %.8f(double)", event);


    printf("%s","=================\nNegative tests\n");
    printf("%s","=================\nLog event using Third format\n");
    log_event("%-50s(stamp), %50s(name, %lo(number), %a(bin), %%, %.8f(double)", event);
    log_event("%(times), %z(name), %lo(number), %a(bin), %%, %.8f(double)", event);
    log_event("%(times), %(name    ), %lo(number), %a(bin), %%, %.8f(double)", event);
    log_event("%(times), (name    ), %lo(number), %a(bin), %%, %.8f(double)", event);
    log_event("%(times), (name    ) %, %.8f(double)", event);
    log_event("%(times), (name    ) %f (double)", event);


    destroy_collection(event);
    close_audit();

    printf("Low level test end\n");
    return error;
}


int low_level_test()
{
    int error = EOK;
    char *sinks[]= { "foo", "stderr", "bar", NULL };
    struct dispatcher_handle *dispatcher;
    struct dispatcher_handle *dispatcher2;

    printf("Low level test start\n");

    error = create_audit_dispatcher(&dispatcher,"my_app",sinks,NULL,NULL);
    if(error) {
        printf("create_audit_dispatcher returned %d\n", error);
        return error;
    }

    printf("%s","=================\nCreating collection\n");
    error = build_event();
    if(error) { 
        printf("Error creating event %d\n",error);
        return error;
    }


    printf("%s","=================\nLogging peer - expect success\n");
    print_collection(peer);
    log_audit_event(dispatcher, NULL, peer);
    printf("%s","=================\nLogging host - expect success\n");
    print_collection(host);
    log_audit_event(dispatcher, NULL, host);
    printf("%s","=================\nLogging socket - expect success\n");
    print_collection(socket);
    log_audit_event(dispatcher, NULL, socket);
    printf("%s","=================\nLogging event - expect success\n"); 
    print_collection(event);
    log_audit_event(dispatcher, NULL, event);

    /* Try to alter list of sinks */
    printf("%s","=================\nSeries of negative test cases.\n");
    /* Deleting non exisintg sink */  
    error = alter_audit_dispatcher(dispatcher,"far2",ELAPI_SINK_ACTION_DELETE);
    if(!error) printf("%s","Expected failure got success\n");
    else printf("Expected failure. Error : %d\n",error);

    /* Editing non exisintg sink */  
    error = alter_audit_dispatcher(dispatcher,"far2",ELAPI_SINK_ACTION_ENABLE);
    if(!error) printf("%s","Expected failure got success\n");
    else printf("Expected failure. Error : %d\n",error);

    /* Adding duplicate */
    error = alter_audit_dispatcher(dispatcher,"bar",ELAPI_SINK_ACTION_ADD);
    if(!error) printf("%s","Expected failure got success\n");
    else printf("Expected failure. Error : %d\n",error);

    /* Adding new */
    printf("%s","=================\nAdding syslog - first time - expect success\n");
    error = alter_audit_dispatcher(dispatcher,"syslog",ELAPI_SINK_ACTION_ADD);
    if(error) printf("Expected success got failure %d\n",error);
    else printf("Success : %d\n",error);

    /* Delete */
    error = alter_audit_dispatcher(dispatcher,"stderr",ELAPI_SINK_ACTION_DELETE);
    if(error) printf("Expected success got failure %d\n",error);
    else printf("Success : %d\n",error);

    /* Enable */
    error = alter_audit_dispatcher(dispatcher,"syslog",ELAPI_SINK_ACTION_ENABLE);
    if(error) printf("Expected success got failure %d\n",error);
    else printf("Success : %d\n",error);

    /* Log event into syslog */
    printf("%s","=================\nLogging peer - expect success\n");
    print_collection(peer);
    log_audit_event(dispatcher, NULL, peer);
    printf("%s","=================\nLogging host - expect success\n");
    print_collection(host);
    log_audit_event(dispatcher, NULL, host);
    printf("%s","=================\nLogging socket - expect success\n");
    print_collection(socket);
    log_audit_event(dispatcher, NULL, socket);
    printf("%s","=================\nLogging event - expect success\n"); 
    print_collection(event);
    log_audit_event(dispatcher, NULL, event);

    /* Pulse */
    error = alter_audit_dispatcher(dispatcher,"syslog",ELAPI_SINK_ACTION_PULSE);
    if(error) printf("Expected success got failure %d\n",error);
    else printf("Success : %d\n",error);

    
    printf("%s","=================\nCreating another dispatcher - expect success\n"); 
    error = create_audit_dispatcher(&dispatcher2,"my_app",sinks,NULL,NULL);
    if(error) printf("Expected success got failure %d\n",error);
    else printf("Success : %d\n",error);

    printf("%s","=================\nAdding syslog sink to it - expect failure\n"); 
    error = alter_audit_dispatcher(dispatcher2,"syslog",ELAPI_SINK_ACTION_ADD);
    if(!error) printf("%s","Expected failure got success\n");
    else printf("Expected failure. Error : %d\n",error);


    /* Delete */
    error = alter_audit_dispatcher(dispatcher,"syslog",ELAPI_SINK_ACTION_DELETE);
    if(error) printf("Expected success got failure %d\n",error);
    else printf("Success : %d\n",error);


    printf("%s","=================\nAdding syslog sink to it - now expect success\n"); 
    error = alter_audit_dispatcher(dispatcher2,"syslog",ELAPI_SINK_ACTION_ADD);
    if(error) printf("Expected success got failure %d\n",error);
    else printf("Success : %d\n",error);

    /* Disable */
    error = alter_audit_dispatcher(dispatcher2,"syslog",ELAPI_SINK_ACTION_DISABLE);
    if(error) printf("Expected success got failure %d\n",error);
    else printf("Success : %d\n",error);

    destroy_audit_dispatcher(dispatcher);
    destroy_audit_dispatcher(dispatcher2);
    destroy_collection(peer);
    destroy_collection(host);
    destroy_collection(socket);
    destroy_collection(event);
    printf("Low level test end\n");
    return error;
}


int main()
{
    int error = EOK;

    error = low_level_test(); 
    printf("Low level test returned: %d\n",error);
    if(error) return error;

    error = high_level_test("my_app1"); 
    printf("High level test first run returned: %d\n",error);
    if(error) return error;

    error = high_level_test("my_app2"); 
    printf("High level test second run returned: %d\n",error);
    if(error) return error;

    error = other_high_level_test(); 
    printf("Other high level test returned: %d\n",error);
    return error;
}

   
 
