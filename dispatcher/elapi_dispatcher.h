/*
    ELAPI

    Header file for the dispatcher interface.

    Copyright (C) Dmitri Pal <dpal@redhat.com> 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ELAPI_DISPATCHER_H
#define ELAPI_DISPATCHER_H

#include "elapi_collection.h"
#include "elapi_sink.h"

/* Status values returned by the routing function */
#define ELAPI_DISPATCHER_SKIP   0
#define ELAPI_DISPATCHER_DONE   1
#define ELAPI_DISPATCHER_NEXT   2
#define ELAPI_DISPATCHER_ERROR  3


/* Actions that can be taken against a sink */
#define ELAPI_SINK_ACTION_ADD       0   /* Add a new sink */
#define ELAPI_SINK_ACTION_DELETE    1   /* Delete a sink */
#define ELAPI_SINK_ACTION_DISABLE   2   /* Disable a sink */
#define ELAPI_SINK_ACTION_PULSE     3   /* Disable a sink temporarily for one call */
#define ELAPI_SINK_ACTION_ENABLE    4   /* Enable sink */


/* Event routing function */
typedef int (*event_router_fn)(char *sink, 
                                char *previous_sink, 
                                int previous_status, 
                                struct collection_item *event,
                                char *format_string,
                                struct sink_descriptor *sink_data,
                                void *custom_data, 
                                int *error); 

struct dispatcher_handle {
    char **sinks;
    char *appname;
    event_router_fn router;
    struct collection_item *sink_list;
    int sink_counter;
    void *custom_data;
};

/******************** Low level thread safe interface ************************************/
/* This interface should be used if application plans to control the dispatcher,
 * implement its own sinks that can be added dynamically or implements it own routing finction.
 */

/* Function to create a dispatcher */
int create_audit_dispatcher(struct dispatcher_handle **dispatcher, /* Handle of the dispatcher will be stored in this variable */
                            const char *appname,                   /* Application name. Passed to the sinks to do initialization */
                            char **desired_sinks,                  /* List of names of the sinks to use. If NULL the default will be used */ 
                            event_router_fn desired_router,        /* The rounting function. If NULL the default will be used */ 
                            void *custom_data);                    /* Custom data that can be used in the router function. */


/* Function to clean memory associated with the audit dispatcher */
void destroy_audit_dispatcher(struct dispatcher_handle *dispatcher);

/* Function to log an event */
void log_audit_event(struct dispatcher_handle *dispatcher, char *format_string, struct collection_item *event);

/* Advanced functions */
/* Managing the sink collection */
int alter_audit_dispatcher(struct dispatcher_handle *dispatcher, /* Dispatcher */
                           char *sink,                           /* Sink to change */
                           int action);                          /* Action to perform for sink */

/* This function is exposed in case you are providing your own routing callback */   
int log_event_to_sink(struct sink_descriptor *sink_data,
                      struct collection_item *event,
                      char *format_string,
                      void *custom_data);


/******************** High level interface ************************************/
/* This interface is not thread safe but hides the dispatcher. */

/* Function to open audit using default routing functions */
/* If appname is NULL - uses module name */
int open_audit(const char *appname, char **desired_sinks);

/* Function to open audit using custom routing function */
int open_audit_with_router(const char *appname, 
                           char **desired_sinks,
                           event_router_fn desired_router,
                           void *custom_data);

/* Log event  */
void log_event(char *format_string,struct collection_item *event);

/* Get dispatcher if you want to add sink to a default deispatcher or do some advaced operations */
struct dispatcher_handle *get_dispatcher(void);

/* Close audit */
void close_audit(void);

/* Creates collection with the timestamp already prepopulated */
int create_event(struct collection_item **event,char *name);

/* Creates collection - event based on the specified format */
int construct_event(struct collection_item **event,char *name, char *format, ...); 

/* Add/Updates the event properties based on the format */ 
int modify_event(struct collection_item *event, int update, char *format, ...); 

#endif
