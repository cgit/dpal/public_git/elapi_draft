/*
    ELAPI

    Implemenation of the stderr sink.

    Copyright (C) Dmitri Pal <dpal@redhat.com> 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <errno.h>
#include <malloc.h>
#include "elapi_sink.h"
#include "elapi_collection.h"
#include "elapi_debug.h"
#include "elapi_util.h"


/***** Standard functions each facility has to provide *****/
/* Initialize facility - open files, establish connnectons, etc... */
static int stderr_sink_init(struct data_descriptor *dblock)
{
    struct serial_data *serialized_data;

    DEBUG_STRING("stderr_sink_init","Entry");

    /* Prepare the block where the format function will store its data */
    errno = 0;
    dblock->internal_data = NULL;
    serialized_data = malloc(sizeof(struct serial_data));
    if(serialized_data == NULL) return errno;

    serialized_data->buffer = NULL;
    serialized_data->size = 0;
    serialized_data->length = 0;
    serialized_data->nest_level = 0;

    dblock->internal_data = (void *)(serialized_data);


    DEBUG_NUMBER("DBLOCK in init",dblock);
    DEBUG_NUMBER("internal data in init",dblock->internal_data);
    DEBUG_STRING("Application name in init:",dblock->appname);

    DEBUG_STRING("stderr_sink_init","Exit");
    return EOK;
}


/* Formatting calback */ 
static int stderr_sink_format(struct data_descriptor *dblock,
                              char *format_str,
                              struct collection_item *event)
{
    struct serial_data *serialized_data;
    int error = EOK;

    DEBUG_STRING("stderr_sink_format","Entry");
    DEBUG_NUMBER("DBLOCK in format",dblock);
    DEBUG_NUMBER("internal data in format",dblock->internal_data);

    serialized_data = (struct serial_data *)(dblock->internal_data);

    if(format_str != NULL) {
        /* Use format string */
        DEBUG_STRING("Using format:",format_str);
        error = serialize_with_format(event,format_str,serialized_data);
    }
    else {
        /* Traverse collection */
        DEBUG_STRING("Using default serialization callback","");
        error = traverse_collection(event,ELAPI_TRAVERSE_DEFAULT | ELAPI_TRAVERSE_END ,serialize,(void *)(serialized_data));
    }
    if(error) {
        DEBUG_NUMBER("Serialization returned error",error);
        return error;
    }

    DEBUG_STRING("stderr_sink_format","Exit");
    return EOK;
}


/* Cleanup per event internal data after a failure */
static void stderr_sink_cleanup(struct data_descriptor *dblock)
{
    struct serial_data *serialized_data;
    DEBUG_STRING("stderr_sink_cleanup","Entry");

    serialized_data = (struct serial_data *)(dblock->internal_data);

    if(serialized_data->buffer != NULL) {
        free(serialized_data->buffer);
        serialized_data->buffer = NULL;
        serialized_data->size = 0;
        serialized_data->length = 0;
        serialized_data->nest_level = 0;
    }

    DEBUG_STRING("stderr_sink_cleanup","Exit");
}

/* Close facility */ 
static void stderr_sink_close(struct data_descriptor *dblock)
{
    DEBUG_STRING("stderr_sink_close","Entry");

    if(dblock->internal_data != NULL) {
        stderr_sink_cleanup(dblock);
        free(dblock->internal_data);
        dblock->internal_data=NULL;
    }

    DEBUG_STRING("stderr_sink_close","Exit");
}


/* Logging calback */ 
static int stderr_sink_submit(struct data_descriptor *dblock)
{
    struct serial_data *serialized_data;
    DEBUG_STRING("stderr_sink_submit","Entry");
    DEBUG_NUMBER("DBLOCK in submit",dblock);
    DEBUG_NUMBER("internal data in submit",dblock->internal_data);

    serialized_data = (struct serial_data *)(dblock->internal_data);

    DEBUG_STRING("OUTPUT:",serialized_data->buffer);

    fprintf(stderr,"%s\n",serialized_data->buffer);

    stderr_sink_cleanup(dblock);

    DEBUG_STRING("stderr_sink_submit","Exit");
    return EOK;
}

/* Return a filled in structure */
void get_sink_info(struct sink_capability *sink_cpb_block)
{
    DEBUG_STRING("get_sink_info","Entry");

    sink_cpb_block->retry_interval = SINK_NEVER_RETRY;
    sink_cpb_block->flags = SINK_FLAG_NO_LIMIT;
    /* Instance never changes for this sink so one can load as many as he needs */
    sink_cpb_block->instance = 0;
    sink_cpb_block->init_cb = stderr_sink_init;
    sink_cpb_block->cleanup_cb = stderr_sink_cleanup;
    sink_cpb_block->format_cb = stderr_sink_format;
    sink_cpb_block->submit_cb = stderr_sink_submit;
    sink_cpb_block->close_cb = stderr_sink_close;


    DEBUG_STRING("get_sink_info","Exit");    
}

